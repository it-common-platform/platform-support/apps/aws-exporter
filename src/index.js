const promBundle = require("express-prom-bundle");
const app = require("express")();
const metricsMiddleware = promBundle({includeMethod: true});
const promClient = require("prom-client");
const { CostExplorerClient, GetCostAndUsageCommand, GetTagsCommand } = require("@aws-sdk/client-cost-explorer");
const costExplorerClient = new CostExplorerClient({ region: process.env.AWS_REGION || "us-east-1" });

const costsMetric = new promClient.Counter({
  name: "aws_estimated_monthly_costs_total",
  help: "The current estimated monthly cost by cost allocation tag, in USD",
  labelNames: ["tagKey", "tagValue"]
});

const lastUpdatedMetric = new promClient.Counter({
  name: "aws_estimated_monthly_costs_last_update_seconds",
  help: "The timestamp of the last update for estimated costs"
});

if (!process.env.TAG_KEYS) {
  throw new Error("No TAG_KEYS defined");
}

function convertNumber(num) {
  return (num < 10) ? "0" + num : num;
}

async function updateMetrics() {
  costsMetric.reset();

  const tags = process.env.TAG_KEYS.split(",");
  await Promise.all(tags.map(updateMetricForTag));
}

async function updateMetricForTag(tagKey) {
  const now = new Date();
  const startDate = `${now.getFullYear()}-${convertNumber(now.getMonth()+1)}-01`;
  const endDate = `${now.getFullYear()}-${convertNumber(now.getMonth()+1)}-${convertNumber(now.getDate())}`;

  const tagCommand = new GetTagsCommand({
    TimePeriod: {
      Start: startDate,
      End: endDate
    },
    TagKey: tagKey
  });

  // No data is available on the first date
  if (startDate === endDate) {
    costsMetric.reset();
    return;
  } else {

    const data = await costExplorerClient.send(tagCommand);
    const tagValues = data.Tags.filter(t => t); // Remove empty strings



    const results = await Promise.allSettled(
      tagValues.map(async (tag) => {
        const getCostCommand = new GetCostAndUsageCommand({
          TimePeriod: {
            Start: startDate,
            End: endDate
          },
          Filter: {
            Tags: {
              Key: tagKey,
              Values: [tag]
            }
          },
          Metrics: ["UnblendedCost"],
          Granularity: "MONTHLY"
        });

        const result = await costExplorerClient.send(getCostCommand);
        return [tag, result];
      }
    ));

    results.forEach(result => {
      const [tag, costExplorerResponse] = result.value;

      costsMetric.inc(
        { tagKey: tagKey, tagValue: tag}, 
        parseFloat(costExplorerResponse.ResultsByTime[0].Total.UnblendedCost.Amount)
      );
    });
  }
  lastUpdatedMetric.reset();
  lastUpdatedMetric.inc(Math.floor(Date.now() / 1000));
}

// Update cost metrics once every four hours
setInterval(updateMetrics, 1000 * 60 * 60 * 4);
updateMetrics();

app.use(metricsMiddleware);
app.listen(process.env.PORT || 3000, function() { 
  console.log(`Listening on port ${this.address().port}` );
});

process.on("SIGINT", () => process.exit(0));
process.on("SIGTERM", () => process.exit(0));
